# README #
YOUR API DOCUMENTATION

### Requirements ###

* Yarn or npm
* node

## Installing ##
* Run either `yarn` or `npm i` in the root folder of the repo

### Compiling ###

* Run either `yarn compile` or `npm run compile` 

### Description ###
In `dev.yaml` you may find raw version of documentation. It uses references to other files. In order to use this documentation with Swagger you have to compile those assets first(HOWTO: see section above) 
